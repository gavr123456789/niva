using Niva;

void main(string[] args){
    Test.init(ref args);
    Test.add_func("/simple_test", simple_test);
    Test.run();
}

void many_file_execution_test(){
    string[] commands = {"print(\"1\");"};
    for (int i = 2; i < 3; i++) commands += @"print(\"$i\");";

    var eval = new Evaluator.with_commands(commands);
    eval.evaluate_many(true);
    foreach (var a in eval.async_results.entries){
        prin(a.key, " => ", a.value);
    }
}
void simple_test(){
    prin(Environment.get_current_dir());
    var program = file_to_string("simple_test.vala");
    var a = new Niva.Sas(program);
    string[] commands = {program};
    var eval = new Evaluator.with_commands(commands);
    eval.evaluate_many(false);
    prin(a.run());
}
void add_brackets_test(){
    string program = """fun void main ()
    var a = 5
    var b = 4
    for (int i = 0; i < a; i++)
        b++
    
    print(@"$b")
    var klass = new Tesst()
    klass.say_yes()
    

class Tesst : Object 
    int a
    int b
    fun Gee.ArrayList<int>  from_code_to_otstups(string[] lines)
        return Seq.of_array<string>(lines)
            .map<int>(g =>  space_counter(g.to_utf8(), g.chug().has_prefix(".")?true:false))
            .collect_ordered(Collectors.to_list())
            .value as Gee.ArrayList<int>
    
""";
    var a = new Niva.Sas(program);
    string[] commands = {program};
    var eval = new Evaluator.with_commands(commands);
    eval.evaluate_many(false);
    prin(a.run());
}
