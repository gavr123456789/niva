fun void main ()
    var a = 5
    var b = 4
    for (int i = 0; i < a; i++)
        b++
    while (a < 8)
        a++
    
    if (a > 5) 
        print (@"$a\n")
    var test_class = new TestClass()
    print(@"test class = $test_class\n")

fun void test_func()
    print("Test func works\n")

class TestClass : Object
    int z = 0
    fun public string to_string()
        return @"$z\n"