int main(string[] args){
    if (args[1] == null) error("You need to pass a file");
    var program = file_to_string(args[1]);
    var niva_core = new Niva.Sas(program);
    var prog_to_exec = niva_core.run();
    Evaluator.eval(prog_to_exec, args[1]);

    return 0;
}