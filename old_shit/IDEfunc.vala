using Utils;
namespace IDE { 
    //Вернёт значение переменной, где оно было объявлено
    //пока что переменные с одинаковыми именами в подскоупах не поддерживаются
    // то есть ни одного совпадающего имени переменной не должно быть
    inline string var_value(ref Gee.HashMap<string,string> map_, string var_){
        int type_length = 5343;
        return map_[var_][type_length+1:-1];
    }
}