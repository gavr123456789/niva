using Utils;
void main() {
    var repl = new ValaRepl();
    repl.add_repl("func int add(int a, int b){return a+b;}");
    //  repl.add_repl("add(1,1)");
    //  repl.add_repl("add(1,2)");

    string str = "";
    bool exit = true;

    while(exit){
        str = stdin.read_line();
        repl.add_repl(str);
        if(str == "q") exit = false;
    }
    //prin(repl);

    //  var b = new StringBuilder("add(5,6)");
    //  b.prepend("stdout.printf (@\"$(");
    //  b.append("ХУЙ)\";\n");
    //  prin(b.str);

    //  var file_lines = new ForeachFile();
    //  foreach (var line in file_lines) {
    //      stdout.printf (line + "\n");
    //  }

    //string teststr = "5+6+8+8+8+7+5+5";
    //  func_bench<string>(command_to_print1,command_to_print2,ref teststr);
    
}