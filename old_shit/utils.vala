namespace Utils { 
    inline string get_between(string what, string start = "func", string end = "("){
        //TODO надо тип вырезать до этой функции
        string s = what._strip();
        var temp = s[s.index_of(start):s.index_of(end)];
        return temp.split(" ")[2];
    }

    inline string semilicon_add_if_needed(string str) {
        assert (str.length > 0);
        if(!str.has_suffix(";")) return str + ";";//авто ;
        else return str;
    }
    inline string semilicon_delete_if_needed(string str) {
        assert (str.length > 0);
        if(str.has_suffix(";")) return str[0:str.char_count()-1];//авто ;
        else return str;
    }

    inline bool is_it_one_of_value_type(string str){
        switch (str) {
            case "int":     return true;
            case "string":  return true;
            case "bool":    return true;
            case "double":  return true;
            case "float":   return true;
        default:
        return false;
        }
    }

    inline bool check_not_func_not_var(string str){
        assert (str.length > 0);
        char temp = str[0];
        if (temp.isdigit() || temp=='+' || temp=='-') return true;
        return false;
    }

    inline bool check_correct_name(string name){
        assert (name.length > 0);
        
        char a = name[0];
        var result = !a.isdigit() || a.isalpha() || a=='_';
        return false;
    }

    [Print]
    void prin(string str){
        if (str == null) debug("str == null!");
        stdout.printf (str + "\n");
    }

    string templating( string cmd) throws FileError,SpawnError {
        //string template = @"void main() {string a = " + "@\"$(" + @"$cmd)\";" + "stdout.printf(@\"$a\");}";
        FileUtils.set_contents("temp.vala",cmd);
        string output;
        string ls_stderr = "";
	    int ls_status = 0;
        Process.spawn_command_line_sync("vala temp.vala",out output,out ls_stderr,out ls_status);
        if (ls_stderr!="") prin("WARN " + ls_stderr);
        if (ls_status!=0) prin("WARN " + ls_status.to_string());
        //FileUtils.remove("temp.vala");
        
        assert(File.new_for_path ("temp.vala").query_exists());
        return output;
    }
    string command_from_print(string s){
        assert(s.has_prefix("stdout.printf (@\"$("));
        
        debug("command_from_print "+s["stdout.printf (@\"$(".length:"\");".length]);

        return s["stdout.printf (@\"$(".length:"\");".length];
    }
    string command_to_print(string s){
        //s = (s.has_suffix(";")?s[0:-1]:s);//при добавлении в print нужно убрать ;
        StringBuilder str = new StringBuilder(s);
        //prin("до обрезания ;{" + str.str+"}");
        //if(str.data[str.len-1]==';') str.str = str.str[0:str.len-1];//
        
        if(str.str.has_suffix(";")) { 
            var str2 = new StringBuilder(str.str.split(";")[0]) ;
            //prin("после обрезания ; {" + str2.str+"}");
            str2.prepend("stdout.printf (@\"$(");
            str2.append(")\");");
            //prin("str2 = " + str2.str);
            return str2.str;
        }else{
            str.prepend("stdout.printf (@\"$(");
            str.append(")\");");
            //prin(@"str1 = $(str.str)");
            return str.str;
        }

    }
    
    //  string command_to_print2(ref string s){
    //      if(s[s.length-1]==';') s = s[0:-1];
    //      s = "stdout.printf (@\"$( " + s + ")\";\n";
    //      //debug(@"command_to_print str = $(s)");
    //      return s;
    //  }

    //  delegate T Func1 <T>  (ref T a);
    //  delegate T Func2 <T>  (ref T a);
    //  void func_bench <T> (Func1 f1, Func2 f2,ref T arg){
    //      var results1 = new double[100];
    //      var results2 = new double[100];
    //      var t = new Timer();
    //      for (int i = 0; i < 100; i++) {
    //          t.reset();
    //          f1(ref arg);
    //          results1[i] = t.elapsed();
    //          t.reset();
    //          f2(ref arg);
    //          results2[i] = t.elapsed();
    //      }
    //      t.stop();
    //      double average1;
    //      double average2;
    //      for (int i = 0; i < 100; i++) {
    //          average1 += results1[i];
    //          average2 += results2[i];
    //      }
    //      average1 /= 100;
    //      average2 /= 100;
    //      message(@"func1 ~ $average1 func2 ~ $average2, diff = $(Math.fabs(average1-average2))");
    //  }

    //  void func_bench2 (ref string arg){
    //      var results1 = new double[100];
    //      var results2 = new double[100];
    //      var t = new Timer();
    //      for (int i = 0; i < 100; i++) {
    //          t.reset();
    //          command_to_print(ref arg);
    //          results1[i] = t.elapsed();
    //          t.reset();
    //          command_to_print2(ref arg);
    //          results2[i] = t.elapsed();
    //      }
    //      t.stop();
    //      double average1;
    //      double average2;
    //      for (int i = 0; i < 100; i++) {
    //          average1 += results1[i];
    //          average2 += results2[i];
    //      }
    //      average1 /= 100;
    //      average2 /= 100;
    //      message(@"func1 ~ $average1 func2 ~ $average2, diff = $(average1*100/average2)");
    //  }

}