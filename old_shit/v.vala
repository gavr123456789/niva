using Gee;
using Utils;
//  struct FileSeeks {
//      int start_main;
//      int end_main;
//  }

[SingleInstance]
class ValaRepl : Object {
    public inline string to_string(){
        var res = new StringBuilder();
        res.append("--vaiables--\n");
        foreach (var item in map_vars_code.entries) 
            res.append(@"\t$(item.key) => $(item.value)\n");
        res.append("\n--functions--\n");
        foreach (var item in map_funcs_code.entries) 
            res.append(@"\t$(item.key) => $(item.value)\n");
        //res.append(@"\b--last_command--\n\t$last_command\n");
        res.append(@"\n--fileContent--\n\t$(fileContent.str)\n");

        return res.str;
    }
    HashMap<string,string> map_vars_code = new HashMap<string,string>();
    HashMap<string,string> map_funcs_code = new HashMap<string,string>();
    StringBuilder fileContent = new StringBuilder();
    StringBuilder main = new StringBuilder();
    ArrayList<int?> line_offsets;
    ArrayList<string> commands = new ArrayList<string>();
    //ArrayList<string> last2commands = new ArrayList<string>();
 
    //  string last_command="";
    //  string pre_last_command="";
    //  int last_command_length;//for deleting it
    //  int pre_last_command_length;

//Возвращает это объявление функции, переменной или другое
    public inline int get_type_of_line ( ref string cmd){
        if (cmd == "") {warning("command.length == 0"); return -1;}
        else if (cmd.has_prefix("func ")){
            debug("func detected");

            if (cmd.length < 5)                 {warning("command.length < 5"); return -1;}
            else if (cmd.split(" ").length < 3) {warning("Syntax Error! < 2 spaces"); return -1;}
            
            return 1;//func
/////////variable//////////
        } else if (is_it_one_of_value_type(cmd.split(" ")[0]) ||
                    cmd.has_prefix("var ")){
            debug("var creating detected");
            return 2;//var
/////////others//////////            
        } else {
            //TODO Добавить сюда добавление функций из массива всех функций которые были
            // ; нужно ставить только для последней команды, потому что она идет в printf;
            
            return 3;//other
        }
    }

    public bool add_repl(string command){
        string cmd = command.strip();
        

/////////funclion//////////1-func, 2-var, 3-command
        switch (get_type_of_line(ref cmd)) {
            case 1: add_func_to_map(cmd); break;
            case 2: {
                cmd = semilicon_add_if_needed(cmd);
                if (is_it_one_of_value_type(cmd.split(" ")[0])) add_to_global_space(cmd);
                //if (cmd.has_prefix("var ")) add_vars_create_to_main
                break;}
            case 3: {
                //TODO Добавить сюда добавление функций из массива всех функций которые были
                // ; нужно ставить только для последней команды, потому что она идет в printf;
                commands.add(cmd);
                add_commands_to_main();
                break;
            }
            case -1: return false; 
        }
        add_all();

        try{ prin(templating(fileContent.str));}
        catch(SpawnError e){
            stderr.printf("SpawnError : %s\n",e.message);
        }
        catch(FileError e){
            stderr.printf("FileError : %s\n",e.message);
        }
        
        return true;
    }
    
    inline void add_func_to_map(string str){
        map_funcs_code[get_between(str)] = str[4:str.length];//map[func name] = func 
    } 
    //в fast моде нужно генерировать 
    //TODO если vars содержит new то добавлять нужно в main
    //TODO затестить что быстрее, стирать из filecontent все и добавлять заного, или стирать только 
    //закрывающую скобку и последнюю команду добавляя 
    inline void add_all(bool fast_mode = false) {
        fileContent.erase();
        fileContent.append("//--functions--\n");
        foreach (var item in map_funcs_code.values) 
            fileContent.append(@"$(item)\n");
        
        fileContent.append("//--main--\n");
        foreach (var item in map_vars_code.values) //Переделать на others
            fileContent.append(@"$(item)\n");
        
        fileContent.append(get_main());
        prin("--------");
        prin(fileContent.str);
    }
    
    inline void add_to_global_space(string str){
        fileContent.prepend(str + "\n");
    }


    inline void add_commands_to_main(){
        main.erase();
        //  prin("~~~");
        //  foreach (var a in commands) prin(a);
        //  prin("~~~");
        if (commands.size > 1){
            //  prin("Предпоследняя команда = " + commands[commands.size-2]);
            for (int i = 0; i < commands.size-1;i++){
                main.append(semilicon_add_if_needed(commands[i]) + "\n");
            }  

            main.append(command_to_print( commands.last()));

            //  prin("---------");
            //  prin(main.str);
            //  prin("---------");

            //сохранить преласт комманд и ласт комманд
            //вырезать с конца их длинну
            //у преласт комманд убрать принт, у ласт комманд добавить
            //добавить измененную преласт комманд и ласт комманд
            //  var main_lines = main.str.split("\n");
            //  pre_last_command = main_lines[main_lines.length-1];
            //  //last_command = main_lines[main_lines.length-1];
            //  prin("pre = " + pre_last_command + "\n" +"last = " + last_command);
            //  prin("--------before truncate---------");
            //  prin(main.str);
            //  prin("--------after truncate----------");
            //  main.truncate(pre_last_command.char_count());
            //  prin(main.str);
            //  prin("-----------------------");
            //  //TODO вместо того чтобы делить сплитом каждый раз проще добавлять в массив
            //  pre_last_command = command_from_print(pre_last_command);
            //  last_command = command_to_print(last_command);
            //  main.append(pre_last_command+"\n").append(last_command+"\n");



            //  string temp = main.str[main.len-last_command_length+21:main.len];
            //  prin(temp.length);
            //  main.truncate(last_command_length+21);
            //  main.append(temp);
            //  prin("--------------------------");
            //  prin(main.str);
            //  prin("--------------------------");
            //  debug(@"main.len = $(main.len) lasncmdlen = $(last_command_length)");
            //  debug(@"dlinna main len - lastcmd  $(main.len-last_command_length)");
            //  debug(@"virezanaya komanda $temp");
        } else{
            //debug("Perviy raz");
            main.append(command_to_print(commands[0]));
            //prin("После первого добавления main\n---" + main.str+"\n-----\n");
        }

        //  debug(@"main.len = $(main.len) lasncmdlen = $(last_command_length)");

        //  debug(@"dlinna main len - lastcmd  $(main.len-last_command_length)");
        //  debug(@"virezanaya komanda $temp");
    }

    inline string get_main (){
        var builder = new StringBuilder(main.str);
        builder.prepend("void main() {\n");
        builder.append("\n}");
        return builder.str;
    }
}

