class ForeachFile {
        FileStream fStr;
        string line;
    public ForeachFile(string filename = "temp.vala") {
        fStr = FileStream.open(filename,"r+");
        line = "";
    }
    public ForeachFile iterator() {return this;}
    public string? next_value(){
        return line!=null? fStr.read_line(): null;
    }
}