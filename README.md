# Niva

Vala repl/preprocessor  
This is not a real language/project, but an experiment in the implementation of some ideas of syntactic sugar.  
My reasoning about this is [here](https://mail.gnome.org/archives/vala-list/2019-August/msg00000.html) and [here](https://gitlab.gnome.org/GNOME/vala/issues/880).


## Features
- [x] Genie like indents instead of scoupes as in Python  
But with Vala syntax preserved  
- [x] no more semilicons  
- [ ] No need to write void in void funcs
- [ ] No need to write () if func has no args
- [ ] No need to write return statement
- [ ] `with` key word from [this issue](https://gitlab.gnome.org/GNOME/vala/issues/880)

## Example
How it looks like right now
```vala
fun void main ()
    var a = 5
    var b = 4
    for (int i = 0; i < a; i++)
        b++
    while (a < 8)
        a++
    
    if (a > 5) 
        print (@"$a\n")
    var test_class = new TestClass()
    print(@"test class = $test_class\n")

fun void test_func()
    print("Test func works\n")

class TestClass : Object
    int z = 0
    fun public string to_string()
        return @"$z\n"

```
will compile to this  
```vala  
void main (){
    var a = 5;
    var b = 4;
    for (int i = 0; i < a; i++){
        b++;
	}

    while (a < 8){
        a++;
	}

    
    if (a > 5) {
        print (@"$a\n");
	}

    var test_class = new TestClass();
    print(@"test class = $test_class\n");
}

void test_func(){
    print("Test func works\n");
}

class TestClass : Object{
    int z = 0;
    public string to_string(){
        return @"$z\n";
}
}

```