using Gpseq;
public class Evaluator {
    public Gee.HashMap<int,string> async_results = new Gee.HashMap<int,string>();
    public string[] commands;
    int atomic_counter = 0;

    public Evaluator(){}
    public Evaluator.with_commands(string[] commands){
        this.commands = commands;
    }
    public void evaluate_many(bool need_main = true)
    requires(commands.length > 0){
        try {
            var seq = Seq.of_array(commands).parallel()
            //  if (need_main)
                //  seq.map<string>(g => {
                //      var builder = new StringBuilder(g);
                //      builder.prepend("void main(){\n");
                //      builder.append("\n}");
                //      return builder.str;
                //  });
            
            .foreach(g => {
                blocking(() => {
                    //  AtomicInt.inc(ref atomic_counter);
                    atomic_counter++;
                    async_results[atomic_counter] = templating(g,"tmp.vala",atomic_counter);
                });
            })
            .wait();
        } catch (Error e){prin(e.message); }
        AtomicInt.set(ref atomic_counter, 0);
    }
    public static void eval (string code, string filename_was){
        print (templating(code, filename_was));
    }
}
// делает vala к команде(коду) помещенному в файлы
string templating( string cmd, string filename_was = "tmp.vala", int filenum = 0) {
    //string template = @"void main() {string a = " + "@\"$(" + @"$cmd)\";" + "stdout.printf(@\"$a\");}";
    string filename = filename_was.split(".")[0] + @"$filenum" + filename_was.split(".")[1];//название файла + цифра + формат
    string output;
    string ls_stderr = "";
    int ls_status = 0;
    //  prin("Execute input:\n",cmd,"\nEnd of input");
    try{
        FileUtils.set_contents(@"$filename", cmd);
    } catch(FileError e){ print(@"FileError: $(e.message)");}
    assert (FileUtils.test(@"$filename", FileTest.EXISTS));
    try{
        Process.spawn_command_line_sync(@"vala $filename",out output,out ls_stderr,out ls_status);
    } catch (SpawnError e) { print(@"SpawnError: $(e.message)");}

    if (ls_stderr!="") print(ls_stderr);
    //  if (ls_status!=0)  error("WARN " + ls_status.to_string());
    FileUtils.remove(@"$filename");
    //  assert( !FileUtils.test(@"$filename", FileTest.EXISTS));
    return output;
}

