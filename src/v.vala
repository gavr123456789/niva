using Gpseq;

namespace Niva {
    public class Sas : Object {

        Gee.ArrayList<int> otstups = new Gee.ArrayList<int>();
        Gee.ArrayList<int> funcs = new Gee.ArrayList<int>();

        string[] lines;


        public Sas (string content) {
            lines = content.split("\n");
        }

        public string run(){
            lines = remove_white_lines(lines);
            lines += "\n";
            int c = 1;
            otstups = from_code_to_otstups(lines);
            //  foreach(var a in otstups) {prin(c++,lines[c-2],":\t",a);}
            //Add round brackets
            add_semilicons();
            add_brackets(otstups, lines);
            funs ();
            //Add prin fun
            return build_line(lines);
        }

        string build_line(string[] lines){
            var builder = new StringBuilder();
            foreach(var a in lines) {
                builder.append(a);
                builder.append("\n");
            }
            return builder.str;
            //FileUtils.set_contents("output.vala",builder.str);
        }

        void funs () throws Error {
            Seq.iterate<int>(0, i => i < lines.length, i => ++i)
                .parallel()
                .foreach(i => {
                    blocking(() => {
                        //  prin(lines[i].chug()," => ",lines[i].chug().has_prefix("fun"));
                        if (lines[i].chug().has_prefix("fun")){
                            funcs.add(i);
                            lines[i] = lines[i].replace("fun ","");
                        }
                    });
                })
                .wait();
        }

// Если 
// не начинается с return и следующаяя строка имеет больший отстуе(для return a+b\n\t+c;) или с точки (для Seq)
// строка не пустая и её первое слово не ключевое
// строка начинается с точки а следующая не начинается с точки (для Seq в котором ; нужна только на последнем действии)
// поставить ;
        void add_semilicons ( ) throws Error {
            Seq.iterate<int>(0, i => i < lines.length, i => ++i)
                .foreach(i => {
                    blocking(() => {
                        if (!(lines[i].chug().has_prefix("return") && (next_indent(i)==1 || lines[i+1].chug().has_prefix(".")))) 
                            if (lines[i].chomp().length != 0 && !is_key_word( lines[i].chug().split(" ")[0]) 
                                && !(lines[i].chug().has_prefix("."))){
                                lines[i] = lines[i] + ";";
                            }
                            if (lines[i].chug().has_prefix(".") && !lines[i+1].chug().has_prefix("."))
                                lines[i] = lines[i] + ";";

                    });
                });    
        }
        //returns 1 if next indent bigger, 0 if equal, -1 if lower
        inline int next_indent (int place_in_code){
            int a = otstups[place_in_code]-otstups[place_in_code+1];
            //  prin(Log.METHOD," place: ",place_in_code," now: ",otstups[place_in_code]," next: ",otstups[place_in_code+1]);
            return a>0?-1:a==0?0:1;
        }

        inline bool is_key_word(string? str){
            if (str != null){
                //  prin("is_key_word: ",str);
                switch (str) {
                    case "if":  return true;
                    case "for":     return true;
                    case "foreach":   return true;
                    case "fun":   return true;
                    case "while":  return true;
                    case "else":    return true;
                    case "class":   return true;
                    case "struct":   return true;
                    case "main":   return true;
                    case "new":   return true;
                    case "requires":   return true;
                    case "ensures":   return true;
                    case "lock":   return true;
                    case "&&":   return true;// для распределенных по нескольким строкам условий
                    case "||":   return true;
                default: {/*prin(str," not key word\n");*/ return false;}
                }
            } else return false;
        }

        // Создает массив номеров отступов по строкам
        Gee.ArrayList<int>  from_code_to_otstups(string[] lines){
            return Seq.of_array<string>(lines)
                .map<int>(g =>  space_counter(g.to_utf8(), g.chug().has_prefix(".")?true:false))
                .collect_ordered(Collectors.to_list())
                .value as Gee.ArrayList<int>;
        }
        //удаляет строки соержащие пробелы, но не пустые строки
        string[] remove_white_lines(string[] str){
            return Seq.of_array(str)
                .filter(g => !(g.strip().length == g.length && g.length == 0))
                .collect_ordered(Collectors.to_generic_array())
                .value.data;
        }

        //добавляет скобки на разницы во вложенности
        void add_brackets (Gee.ArrayList<int> otstups,  string[] lines){
            for (int i = 0; i < otstups.size-1; i++){
                if (otstups[i] > otstups[i+1]) 
                    lines[i] = lines[i] + add_brackets_on_diffetence(otstups[i], otstups[i+1]);
                else if (otstups[i] < otstups[i+1]) 
                    lines[i] = lines[i] + "{";
            }
        }
    }

    //подсчитывает вложенность
    int space_counter(char[] arr, bool has_dot = false){
        var counter = 0;
        foreach (unowned char a in arr){
            if (a == ' ') ++counter;
            else break;
        }
        if (!has_dot) return counter/4;
        else return counter/4 - 1;
    }

    //добавляет закрывающие скобки в зависимости от глубины вложенности
    string add_brackets_on_diffetence(int a, int b){
        var build = new StringBuilder("\n"); 
        //  prin(a," ",b);
        if (b == -1) {
            build.append("}");
            return build.str;
        } else {
            for (int i = a - b; i > 0; i--){
                //  build.append(string.nfill(i!=0?i-1:i,'\t'));
                build.append(string.nfill(b==0?0:i,'\t'));
                build.append("}\n");
            }
            return build.str;
        }
    }

}


//  void main (){
//      new Sas("test.vala");
//  }