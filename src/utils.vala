public string file_to_string(string filename){
    string s = "";
    try{
    FileUtils.get_contents(filename, out s);
    } catch (Error e){
        error(@"File error: $(e.message)");
    }
    return s;
}

[Print]
public void prin(string s){
    print(s + "\n");
}